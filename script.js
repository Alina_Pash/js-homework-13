/*Теоретичні питання

1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval(`)`.

'setTimeout' і 'setInterval' - це функції JavaScript, які використовуються для планування виконання коду на пізніший час.
Однак вони відрізняються тим, що обробляють час і повторення виконання коду.
'setTimeout' викликає функцію один раз через певний інтервал часу. Він приймає два аргументи: функцію, яку потрібно
виконати і затримку в мілісекундах. Після затримки зазначена функція виконується один раз, а таймер скидається. Простими
словами 'setTimeout' - це одноразовий таймер.
'setInterval' викликає функцію регулярно, повторюючи виклик через певний інтервал часу. Приймає два аргументи: функцію,
яку потрібно виконати, та інтервал у мілісекундах. Функція спочатку використовується з указаним інтервалом, а потім
продовжує працювати з тим самим інтервалом, доки не буде явно зупинено, тобто це повторюваний таймер.

2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку? Чи спрацює вона миттєво і чому?

Якщо в функцію 'setTimeout' передати нульову затримку, то це планує виконання функції якнайшвидше, але планувальник
викликає його лише після завершення виконання поточного скрипту.
Варто пам'ятати, що нульова затримка насправді не дорівнює нулю (у браузері).
'setTimeout' викликає перепланований себе з нульовою затримкою. Кожен виклик запам'ятовує реальний час з попереднього
виклику в масиві times. Перші таймери запускаються негайно, а потім у гру вступає обов'язкова затримка між викликами 4+
мс і ми бачимо 9, 15, 20, 24...

3. Чому важливо не забувати викликати функцію `clearInterval()`, коли раніше створений цикл запуску вам вже не потрібен?

Важливість виклику функції 'clearInterval()' полягає в тому, що коли ви викликаєте функцію 'setInterval' він продовжує
працювати необмежено довго, поки ви явно не зупините це за допомогою 'clearInterval()'. Якщо забути очистити інтервал,
то це може призвести до непотрібного споживання ресурсів, оскільки інтервал продовжуватиме виконувати свій код
неодноразово, споживаючи ресурси процесора в пам'яті.
 */
const images = document.querySelectorAll('.image-to-show');
const stopButton = document.getElementById('stopButton');
const resumeButton = document.getElementById('resumeButton');
let currentImageIndex = 0;
let intervalId;
function showImage(index) {
    images.forEach((image, i) => {
        if (i === index) {
            image.style.display = 'block';
        } else {
            image.style.display = 'none';
        }
    });
}
function showNextImage() {
    currentImageIndex = (currentImageIndex + 1) % images.length;
    showImage(currentImageIndex);
}
function startSlideshow() {
    showImage(currentImageIndex);
    intervalId = setInterval(showNextImage, 3000);
}
function stopSlideshow() {
    clearInterval(intervalId);
}
function resumeSlideshow() {
    startSlideshow();
}
stopButton.addEventListener('click', () => {
    stopSlideshow();
});
resumeButton.addEventListener('click', () => {
    resumeSlideshow();
});
startSlideshow();
